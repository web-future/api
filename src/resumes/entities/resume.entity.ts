import { Job } from '@jobs/entities/job.entity';
import { User } from '@users/entities/user.entity';
import { IUser } from '@users/users.interface';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

export enum Status {
  Pending = 0,
  Accept = 1,
  Reject = 2,
}

@Entity('resumes')
export class Resume {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  email: string;

  @Column({ length: 100, nullable: true })
  fullname: string;

  @Column({ nullable: true, type: 'text' })
  fileUrl: string;

  @Column({ type: 'enum', enum: Status, default: Status.Pending })
  status: Status;

  @ManyToOne(() => Job, (job) => job.resumes)
  job: Job;

  @ManyToOne(() => User, (user) => user.resumes)
  sendBy: IUser;

  @CreateDateColumn({ nullable: true })
  createdAt: Date;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @DeleteDateColumn({ nullable: true })
  deletedAt: Date;
}
