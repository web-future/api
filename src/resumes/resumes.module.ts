import { Module } from '@nestjs/common';
import { ResumesService } from '@resumes/resumes.service';
import { ResumesController } from '@resumes/resumes.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Resume } from '@resumes/entities/resume.entity';
import { Job } from '@jobs/entities/job.entity';
import { GatewayModule } from '@/gateway/gateway.module';
import { NotificationsModule } from '@/notifications/notifications.module';
import { PusherService } from '@/pusher/pusher.service';
import { Company } from '@companies/entities/company.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Resume, Job, Company]),
    GatewayModule,
    NotificationsModule,
  ],
  controllers: [ResumesController],
  providers: [ResumesService, PusherService],
})
export class ResumesModule {}
