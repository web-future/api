import { Company } from '@/companies/entities/company.entity';
import { Message } from '@/messages/entities/message.entity';
import { Notification } from '@/notifications/entities/notification.entity';
import { Post } from '@/posts/entities/post.entity';
import { Chat } from '@chats/entities/chat.entity';
import { Job } from '@jobs/entities/job.entity';
import { Resume } from '@resumes/entities/resume.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToMany,
  OneToMany,
  OneToOne,
  JoinColumn,
} from 'typeorm';

export enum GenderOption {
  MALE = 'male',
  FEMALE = 'female',
}

export enum Role {
  User = 'user',
  Employer = 'employer',
  Admin = 'admin',
}

@Entity('users')
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 100, nullable: true })
  fullname: string;

  @Column({ unique: true })
  email: string;

  @Column({ type: 'text', nullable: true, select: false })
  password: string;

  @Column({ type: 'enum', enum: GenderOption, default: GenderOption.FEMALE })
  gender: GenderOption;

  @Column({ nullable: true })
  phoneNumber: string;

  @Column({ type: 'text', nullable: true })
  avatar: string;

  @Column({ type: 'enum', enum: Role, default: Role.User })
  role: Role;

  @OneToOne(() => Company, (company) => company.hr)
  @JoinColumn()
  company: Company;

  @Column({ type: 'text', nullable: true, select: false })
  refreshToken: string;

  @Column({ type: 'boolean', nullable: true, default: false })
  isVerify: boolean;

  @Column({ length: 50, default: 'local' })
  authProvider: string;

  @ManyToMany(() => Job, (job) => job.liked)
  jobLiked: Job[];

  @OneToMany(() => Post, (post) => post.owner)
  posts: Post[];

  @ManyToMany(() => Post, (post) => post.likedList)
  likedPostList: Post[];

  @ManyToMany(() => Post, (post) => post.bookmarkList)
  bookmarkPostList: Post[];

  @OneToMany(() => Resume, (cv) => cv.sendBy)
  resumes: Resume;

  @OneToMany(() => Notification, (noti) => noti.recipient)
  notis: Notification;

  @ManyToMany(() => Chat, (chat) => chat.members)
  chats: Chat[];

  @ManyToMany(() => Message, (mess) => mess.seenBy)
  seenMessages: Message[];

  @CreateDateColumn({ nullable: true })
  createdAt: Date;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @DeleteDateColumn({ nullable: true })
  deletedAt: Date;
}
