import { PartialType } from '@nestjs/mapped-types';
import { CreateUserDto, CreateEmployerDto } from '@users/dto/create-user.dto';

export class UpdateUserDto extends PartialType(CreateUserDto) {}
export class UpdateEmployerDto extends PartialType(CreateEmployerDto) {}
