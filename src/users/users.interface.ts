export interface IUser {
  id: number;
  fullname: string;
  email: string;
  password: string;
  avatar: string;
  role: string;
  refreshToken: string;
  createdAt: Date;
  updatedAt: Date;
}
