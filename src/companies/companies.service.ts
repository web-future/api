import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateCompanyDto } from '@companies/dto/create-company.dto';
import { UpdateCompanyDto } from '@companies/dto/update-company.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Company } from '@companies/entities/company.entity';
import { Like, Not, Repository } from 'typeorm';
import aqp from 'api-query-params';
import { IUser } from '@users/users.interface';
import { NestCrawlerService } from 'nest-crawler';
import { provinceData } from '@/data/province';

@Injectable()
export class CompaniesService {
  constructor(
    @InjectRepository(Company) private companiesRepository: Repository<Company>,
    private readonly crawler: NestCrawlerService,
  ) {}

  async create(createCompanyDto: CreateCompanyDto) {
    const isExist = await this.companiesRepository.findOne({
      where: {
        companyName: createCompanyDto.companyName.toUpperCase(),
      },
      withDeleted: true,
    });
    if (isExist) {
      throw new BadRequestException('Company already exist!');
    }
    try {
      return await this.companiesRepository.save({
        ...createCompanyDto,
        companyName: createCompanyDto.companyName.toUpperCase(),
      });
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async findAll(currentPage: number, limit: number, queryString: string) {
    const { filter } = aqp(queryString);
    delete filter.current;
    delete filter.pageSize;

    filter.companyName = filter.companyName ? filter.companyName : '';
    filter.provinceName = filter.provinceName ? filter.provinceName : '';
    filter.districtName = filter.districtName ? filter.districtName : '';
    filter.orderBy = filter.orderBy ? filter.orderBy : 'DESC';

    const defaultLimit = +limit ? +limit : 10;
    const offset = ((+currentPage || 1) - 1) * +defaultLimit;
    const totalItems = (
      await this.companiesRepository.find({
        where: {
          companyName: Like(`%${filter.companyName}%`),
          provinceName: Like(`%${filter.provinceName}%`),
          districtName: Like(`%${filter.districtName}%`),
        },
      })
    ).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);

    const companies = await this.companiesRepository.find({
      where: {
        companyName: Like(`%${filter.companyName}%`),
        provinceName: Like(`%${filter.provinceName}%`),
        districtName: Like(`%${filter.districtName}%`),
      },
      order: {
        createdAt: filter.orderBy,
      },
      skip: offset,
      take: defaultLimit,
      relations: ['hr', 'jobs'],
    });
    return {
      data: companies,
      meta: {
        current: currentPage || 1,
        pageSize: defaultLimit,
        pages: totalPages,
        total: totalItems,
      },
    };
  }

  async findOne(id: number) {
    try {
      const company = await this.companiesRepository.findOne({
        where: { id },
        relations: ['hr', 'jobs.category'],
      });
      return company;
    } catch (error) {
      console.log(error);
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async update(id: number, updateCompanyDto: UpdateCompanyDto) {
    const isExist = await this.companiesRepository.findOne({
      where: {
        companyName: updateCompanyDto.companyName.toUpperCase(),
        id: Not(id),
      },
      withDeleted: true,
    });
    if (isExist) {
      throw new BadRequestException('Company already exist!');
    }
    try {
      const company = await this.companiesRepository.findOneBy({ id });

      return await this.companiesRepository.save({
        ...company,
        ...updateCompanyDto,
        companyName: updateCompanyDto.companyName.toUpperCase(),
      });
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  remove(id: number) {
    try {
      this.companiesRepository.softDelete(id);
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async scrape() {
    const data: any = await this.crawler.fetch({
      target: 'https://www.topcv.vn/cong-ty',
      fetch: {
        jobs: {
          listItem: 'div.row div.col-md-4.col-sm-6',
          data: {
            companyName: {
              selector: '.box-company .company-info h3 a',
            },
            introduction: {
              selector: '.box-company .company-info .company-description p',
            },
            logo: {
              selector:
                '.box-company .company-banner div.company-logo a img.img-fluid',
              attr: 'src',
            },
          },
        },
      },
    });

    return data.jobs.map((item) => ({
      ...item,
      provinceName: `${
        provinceData[Math.floor(Math.random() * 100)]
          ? provinceData[Math.floor(Math.random() * 100)].province_name
          : ''
      }`,
    }));
  }
}
