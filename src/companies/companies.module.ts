import { Module } from '@nestjs/common';
import { CompaniesService } from '@/companies/companies.service';
import { CompaniesController } from '@/companies/companies.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Company } from '@/companies/entities/company.entity';
import { NestCrawlerModule } from 'nest-crawler';

@Module({
  imports: [TypeOrmModule.forFeature([Company]), NestCrawlerModule],
  controllers: [CompaniesController],
  providers: [CompaniesService],
})
export class CompaniesModule {}
