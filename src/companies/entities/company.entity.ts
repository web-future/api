import { Notification } from '@/notifications/entities/notification.entity';
import { Job } from '@jobs/entities/job.entity';
import { User } from '@users/entities/user.entity';
import { IUser } from '@users/users.interface';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('companies')
export class Company {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  companyName: string;

  @Column({ nullable: true })
  provinceName: string;

  @Column({ nullable: true })
  districtName: string;

  @Column({ type: 'text', nullable: true })
  introduction: string;

  @Column({ type: 'text', nullable: true })
  logo: string;

  @Column({ nullable: true })
  scales: string;

  @OneToOne(() => User, (user) => user.company)
  hr: User;

  @OneToMany(() => Job, (job) => job.company)
  jobs: Job[];

  @OneToMany(() => Notification, (noti) => noti.sender)
  notis: Notification;

  @CreateDateColumn({ nullable: true })
  createdAt: Date;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @DeleteDateColumn({ nullable: true })
  deletedAt: Date;
}
