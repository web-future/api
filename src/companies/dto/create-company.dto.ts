import { IsNotEmpty } from 'class-validator';

export class CreateCompanyDto {
  @IsNotEmpty()
  companyName: string;

  @IsNotEmpty()
  provinceName: string;

  @IsNotEmpty()
  districtName: string;

  introduction: string;

  logo: string;

  scales: string;
}
