import { Company } from '@companies/entities/company.entity';
import { User } from '@users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('notifications')
export class Notification {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true, type: 'text' })
  content: string;

  @ManyToOne(() => Company, (comp) => comp.notis)
  sender: Company;

  @ManyToOne(() => User, (user) => user.notis)
  recipient: User;

  @Column({ type: 'boolean', default: false })
  isRead: boolean;

  @CreateDateColumn({ nullable: true })
  createdAt: Date;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;
}
