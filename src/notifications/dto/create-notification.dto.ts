import { IsNotEmpty } from 'class-validator';

export class CreateNotificationDto {
  @IsNotEmpty()
  content: string;

  @IsNotEmpty()
  sender: number;

  @IsNotEmpty()
  recipient: number;

  isRead: boolean;
}
