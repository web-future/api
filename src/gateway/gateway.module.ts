import { Module } from '@nestjs/common';
import { Gateway } from '@/gateway/gateway';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Company } from '@companies/entities/company.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Company])],
  providers: [Gateway],
  exports: [Gateway],
})
export class GatewayModule {}
