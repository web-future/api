import { Company } from '@companies/entities/company.entity';
import { Job } from '@jobs/entities/job.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Role, User } from '@users/entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(Company) private companiesRepository: Repository<Company>,
    @InjectRepository(Job) private jobsRepository: Repository<Job>,
    @InjectRepository(User) private usersRepository: Repository<User>,
  ) {}

  async getTotals() {
    const jobsCount = await this.jobsRepository.count();
    const companiesCount = await this.companiesRepository.count();
    const usersCount = await this.usersRepository.count({
      where: { role: Role.User },
    });
    const employersCount = await this.usersRepository.count({
      where: { role: Role.Employer },
    });
    return { jobsCount, companiesCount, usersCount, employersCount };
  }
}
