import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateMessageDto } from '@/messages/dto/create-message.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Message } from '@/messages/entities/message.entity';
import { Repository } from 'typeorm';
import { Chat } from '@chats/entities/chat.entity';
import { IUser } from '@users/users.interface';
import { User } from '@users/entities/user.entity';
import { PusherService } from '@/pusher/pusher.service';

@Injectable()
export class MessagesService {
  constructor(
    @InjectRepository(Message)
    private messagesRepository: Repository<Message>,
    @InjectRepository(Chat)
    private chatsRepository: Repository<Chat>,
    @InjectRepository(User)
    private readonly pusherService: PusherService,
  ) {}

  async create(createMessageDto: CreateMessageDto, user: IUser) {
    try {
      const chat = await this.chatsRepository.findOne({
        where: { id: +createMessageDto.chatId },
        relations: ['members', 'messages'],
      });
      const newMessage = this.messagesRepository.create({
        text: createMessageDto.text,
        image: createMessageDto.image,
        senderId: `${user.id}`,
      });
      newMessage.addUserSeen(user);
      newMessage.chat = chat;
      await this.messagesRepository.save(newMessage);
      await this.chatsRepository.update(
        { id: chat.id },
        {
          lastMessageAt: new Date(),
        },
      );

      // Pusher
      this.pusherService.messagesNew(`${chat.id}`, newMessage);
      const lastMessage = chat.messages[chat.messages.length - 1];
      chat.members.forEach((member) => {
        this.pusherService.chatUpdate(
          member,
          `${createMessageDto.chatId}`,
          lastMessage,
        );
      });

      return newMessage;
    } catch (error) {
      console.log(error);
      throw new BadRequestException('Server failure! Try again');
    }
  }

  findAll() {
    return `This action returns all messages`;
  }

  findOne(id: number) {
    return `This action returns a #${id} message`;
  }

  async getMessagesByChat(chatId: number) {
    try {
      const messages = await this.messagesRepository.find({
        where: {
          chat: { id: +chatId },
        },
        relations: ['seenBy'],
      });
      return messages;
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  remove(id: number) {
    return `This action removes a #${id} message`;
  }
}
