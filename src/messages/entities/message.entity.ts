import { Chat } from '@chats/entities/chat.entity';
import { User } from '@users/entities/user.entity';
import { IUser } from '@users/users.interface';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('messages')
export class Message {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Chat, (chat) => chat.messages)
  chat: Chat;

  @Column({ nullable: true })
  senderId: string;

  @Column({ type: 'text', nullable: true })
  text: string;

  @Column({ type: 'text', nullable: true })
  image: string;

  @ManyToMany(() => User, (user) => user.seenMessages)
  @JoinTable()
  seenBy: IUser[];

  addUserSeen(user: IUser) {
    if (this.seenBy === undefined || this.seenBy === null) {
      this.seenBy = new Array<IUser>();
    }
    this.seenBy.push(user);
  }

  @CreateDateColumn({ nullable: true })
  createdAt: Date;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;
}
