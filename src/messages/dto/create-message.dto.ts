import { IsNotEmpty } from 'class-validator';

export class CreateMessageDto {
  @IsNotEmpty()
  chatId: string;

  text: string;

  image: string;
}
