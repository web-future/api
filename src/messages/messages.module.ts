import { Module } from '@nestjs/common';
import { MessagesService } from '@/messages/messages.service';
import { MessagesController } from '@/messages/messages.controller';
import { Message } from '@/messages/entities/message.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ChatsModule } from '@chats/chats.module';
import { Chat } from '@chats/entities/chat.entity';
import { User } from '@users/entities/user.entity';
import { GatewayModule } from '@/gateway/gateway.module';
import { PusherService } from '@/pusher/pusher.service';
import { Company } from '@companies/entities/company.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Message, Chat, User, Company]),
    ChatsModule,
    GatewayModule,
  ],
  controllers: [MessagesController],
  providers: [MessagesService, PusherService],
})
export class MessagesModule {}
