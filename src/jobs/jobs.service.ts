import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateJobDto } from '@jobs/dto/create-job.dto';
import { UpdateJobDto } from '@jobs/dto/update-job.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Job } from '@jobs/entities/job.entity';
import { Like, Raw, Repository } from 'typeorm';
import { Company } from '@companies/entities/company.entity';
import aqp from 'api-query-params';
import { IUser } from '@users/users.interface';
import { Category } from '@/categories/entities/category.entity';

@Injectable()
export class JobsService {
  constructor(
    @InjectRepository(Job) private jobsRepository: Repository<Job>,
    @InjectRepository(Company) private companiesRepository: Repository<Company>,
    @InjectRepository(Category)
    private categoriesRepository: Repository<Category>,
  ) {}

  async create(createJobDto: CreateJobDto) {
    try {
      const company = await this.companiesRepository.findOneBy({
        id: createJobDto.company,
      });
      const category = await this.categoriesRepository.findOneBy({
        id: createJobDto.category,
      });
      delete createJobDto.company;
      const job = this.jobsRepository.create({
        name: createJobDto.name,
        description: createJobDto.description,
        skills: createJobDto.skills,
        salary: createJobDto.salary,
        quantity: createJobDto.quantity,
        location: createJobDto.location,
        level: createJobDto.level,
        startDate: createJobDto.startDate,
        endDate: createJobDto.endDate,
        isActive: createJobDto.isActive,
      });
      job.company = company;
      job.category = category;
      await this.jobsRepository.save(job);
      return 'OK';
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async findAll(currentPage: number, limit: number, queryString: string) {
    const { filter } = aqp(queryString);
    delete filter.current;
    delete filter.pageSize;

    filter.skill = filter.skill ? `${filter.skill}` : '';

    filter.name = filter.name ? `${filter.name}` : '';

    filter.location = filter.location ? `${filter.location}` : '';

    filter.companyId = filter.companyId ? filter.companyId : false;

    filter.orderBy = filter.orderBy ? filter.orderBy : 'DESC';

    const defaultLimit = +limit ? +limit : 10;
    const offset = ((+currentPage || 1) - 1) * +defaultLimit;
    const totalItems = (
      filter.companyId
        ? await this.jobsRepository.find({
            where: [
              {
                skills: Like(`%${filter.skill.trim()}%`),
                location: Like(`%${filter.location.trim()}%`),
                isActive: true,
                company: { id: filter.companyId },
                endDate: Raw((alias) => `${alias} > NOW()`),
              },
              {
                name: Like(`%${filter.name.trim()}%`),
                location: Like(`%${filter.location.trim()}%`),
                isActive: true,
                company: { id: filter.companyId },
                endDate: Raw((alias) => `${alias} > NOW()`),
              },
            ],
          })
        : await this.jobsRepository.find({
            where: [
              {
                skills: Like(`%${filter.skill.trim()}%`),
                location: Like(`%${filter.location.trim()}%`),
                isActive: true,
                endDate: Raw((alias) => `${alias} > NOW()`),
              },
              {
                name: Like(`%${filter.name.trim()}%`),
                location: Like(`%${filter.location.trim()}%`),
                isActive: true,
                endDate: Raw((alias) => `${alias} > NOW()`),
              },
            ],
          })
    ).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);

    const jobs = filter.companyId
      ? await this.jobsRepository.find({
          where: [
            {
              skills: Like(`%${filter.skill.trim()}%`),
              location: Like(`%${filter.location.trim()}%`),
              isActive: true,
              company: { id: filter.companyId },
              endDate: Raw((alias) => `${alias} > NOW()`),
            },
            {
              name: Like(`%${filter.name.trim()}%`),
              location: Like(`%${filter.location.trim()}%`),
              isActive: true,
              company: { id: filter.companyId },
              endDate: Raw((alias) => `${alias} > NOW()`),
            },
          ],
          order: {
            createdAt: filter.orderBy,
          },
          skip: offset,
          take: defaultLimit,
          relations: ['company', 'category'],
        })
      : await this.jobsRepository.find({
          where: [
            {
              skills: Like(`%${filter.skill.trim()}%`),
              location: Like(`%${filter.location.trim()}%`),
              isActive: true,
              endDate: Raw((alias) => `${alias} > NOW()`),
            },
            {
              name: Like(`%${filter.name.trim()}%`),
              location: Like(`%${filter.location.trim()}%`),
              isActive: true,
              endDate: Raw((alias) => `${alias} > NOW()`),
            },
          ],
          order: {
            createdAt: filter.orderBy,
          },
          skip: offset,
          take: defaultLimit,
          relations: ['company', 'category'],
        });
    return {
      data: jobs,
      meta: {
        current: currentPage || 1,
        pageSize: defaultLimit,
        pages: totalPages,
        total: totalItems,
      },
    };
  }

  async findAllByCate(
    id: number,
    currentPage: number,
    limit: number,
    queryString: string,
  ) {
    const { filter } = aqp(queryString);
    delete filter.current;
    delete filter.pageSize;

    filter.orderBy = filter.orderBy ? filter.orderBy : 'DESC';

    const defaultLimit = +limit ? +limit : 10;
    const offset = ((+currentPage || 1) - 1) * +defaultLimit;

    const totalItems = (
      await this.jobsRepository.find({
        where: {
          category: { id },
          endDate: Raw((alias) => `${alias} > NOW()`),
        },
      })
    ).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);

    const jobs = await this.jobsRepository.find({
      where: {
        category: { id },
        endDate: Raw((alias) => `${alias} > NOW()`),
      },
      order: {
        createdAt: filter.orderBy,
      },
      skip: offset,
      take: defaultLimit,
      relations: ['company', 'category'],
    });

    return {
      data: jobs,
      meta: {
        current: currentPage || 1,
        pageSize: defaultLimit,
        pages: totalPages,
        total: totalItems,
      },
    };
  }

  async findAllJobs(currentPage: number, limit: number, queryString: string) {
    const { filter } = aqp(queryString);
    delete filter.current;
    delete filter.pageSize;

    const defaultLimit = +limit ? +limit : 10;
    const offset = ((+currentPage || 1) - 1) * +defaultLimit;
    const totalItems = (
      filter.companyId
        ? await this.jobsRepository.find({
            where: {
              company: { id: filter.companyId },
            },
          })
        : await this.jobsRepository.find()
    ).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);

    const jobs = filter.companyId
      ? await this.jobsRepository.find({
          where: {
            company: { id: filter.companyId },
          },
          order: {
            createdAt: 'DESC',
          },
          skip: offset,
          take: defaultLimit,
          relations: ['company', 'category'],
        })
      : await this.jobsRepository.find({
          order: {
            createdAt: 'DESC',
          },
          skip: offset,
          take: defaultLimit,
          relations: ['company', 'category'],
        });
    return {
      data: jobs,
      meta: {
        current: currentPage || 1,
        pageSize: defaultLimit,
        pages: totalPages,
        total: totalItems,
      },
    };
  }

  async findOne(id: number) {
    try {
      const job = await this.jobsRepository.findOne({
        where: { id },
        relations: ['company', 'liked', 'category'],
      });
      return job;
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async update(id: number, updateJobDto: UpdateJobDto) {
    try {
      const {
        name,
        description,
        salary,
        skills,
        quantity,
        level,
        startDate,
        endDate,
        location,
        isActive,
      } = updateJobDto;
      const job = await this.jobsRepository.findOneBy({ id });
      return await this.jobsRepository.save({
        ...job,
        name,
        description,
        salary,
        skills,
        quantity,
        level,
        startDate,
        endDate,
        location,
        isActive,
      });
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  remove(id: number) {
    try {
      this.jobsRepository.softDelete(id);
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async like(id: number, user: IUser) {
    try {
      const job = await this.jobsRepository.findOne({
        where: { id },
        relations: ['liked'],
      });
      job.addUserLiked(user);
      return await this.jobsRepository.save(job);
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async unlike(id: number, user: IUser) {
    try {
      const job = await this.jobsRepository.findOne({
        where: { id },
        relations: ['liked'],
      });
      job.liked = job.liked.filter((item) => item.id !== user.id);
      return await this.jobsRepository.save(job);
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async sended(page: number, limit: number, queryString: string, user: IUser) {
    const { filter } = aqp(queryString);
    delete filter.current;
    delete filter.pageSize;

    filter.orderBy = filter.orderBy ? filter.orderBy : 'DESC';
    const defaultLimit = +limit ? +limit : 10;
    const offset = ((+page || 1) - 1) * +defaultLimit;

    const totalItems = (
      await this.jobsRepository.find({
        where: {
          resumes: { sendBy: user },
        },
      })
    ).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);
    const jobs = await this.jobsRepository.find({
      where: {
        resumes: { sendBy: user },
      },
      order: {
        createdAt: filter.orderBy,
      },
      skip: offset,
      take: defaultLimit,
    });

    return {
      data: jobs,
      meta: {
        current: page || 1,
        pageSize: defaultLimit,
        pages: totalPages,
        total: totalItems,
      },
    };
  }
}
