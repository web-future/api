import { PartialType } from '@nestjs/swagger';
import { CreateJobDto } from '@jobs/dto/create-job.dto';

export class UpdateJobDto extends PartialType(CreateJobDto) {}
