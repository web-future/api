import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { CategoriesService } from '@/categories/categories.service';
import { ApiTags } from '@nestjs/swagger';
import { CreateCategoryDto } from '@/categories/dto/create-category.dto';
import { Public } from '@/decorator/customize';
import { RolesGuard } from '@/guards/roles.guard';
import { Roles } from '@/decorator/roles';
import { Role } from '@users/entities/user.entity';

@ApiTags('categories')
@Controller('categories')
export class CategoriesController {
  constructor(private readonly categoriesService: CategoriesService) {}

  @Post()
  @UseGuards(RolesGuard)
  @Roles(Role.Employer, Role.Admin)
  create(@Body() category: CreateCategoryDto) {
    return this.categoriesService.create(category);
  }

  @Public()
  @Get()
  findAll(
    @Query('page') currentPage: string,
    @Query('limit') limit: string,
    @Query() queryString: string,
  ) {
    return this.categoriesService.findAll(+currentPage, +limit, queryString);
  }

  @Public()
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.categoriesService.findOne(+id);
  }
}
