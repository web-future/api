import { Module } from '@nestjs/common';
import { CategoriesService } from '@/categories/categories.service';
import { CategoriesController } from '@/categories/categories.controller';
import { Category } from '@/categories/entities/category.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Category])],
  controllers: [CategoriesController],
  providers: [CategoriesService],
})
export class CategoriesModule {}
