import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from '@/categories/entities/category.entity';
import { Repository } from 'typeorm';
import { CreateCategoryDto } from '@/categories/dto/create-category.dto';
import aqp from 'api-query-params';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectRepository(Category)
    private categoriesRepository: Repository<Category>,
  ) {}

  async create(category: CreateCategoryDto) {
    try {
      return await this.categoriesRepository.save({
        label: category.label,
        icon: category.icon,
      });
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async findAll(currentPage: number, limit: number, queryString: string) {
    const { filter } = aqp(queryString);
    delete filter.current;
    delete filter.pageSize;

    const defaultLimit = +limit ? +limit : 10;
    const offset = ((+currentPage || 1) - 1) * +defaultLimit;
    const totalItems = (await this.categoriesRepository.find()).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);

    const cates = await this.categoriesRepository.find({
      order: {
        createdAt: 'DESC',
      },
      skip: offset,
      take: defaultLimit,
      relations: ['jobs'],
    });
    return {
      data: cates,
      meta: {
        current: currentPage || 1,
        pageSize: defaultLimit,
        pages: totalPages,
        total: totalItems,
      },
    };
  }

  async findOne(id: number) {
    try {
      const cate = await this.categoriesRepository.findOne({
        where: { id },
        relations: ['jobs'],
      });
      return cate;
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }
}
