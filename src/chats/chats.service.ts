import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { CreateChatDto } from '@chats/dto/create-chat.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Chat } from '@chats/entities/chat.entity';
import { Repository } from 'typeorm';
import { User } from '@users/entities/user.entity';
import { IUser } from '@users/users.interface';
import { Message } from '@/messages/entities/message.entity';
import { PusherService } from '@/pusher/pusher.service';

@Injectable()
export class ChatsService {
  constructor(
    @InjectRepository(Chat)
    private chatsRepository: Repository<Chat>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(Message)
    private messagesRepository: Repository<Message>,
    private readonly pusherService: PusherService,
  ) {}

  async create(createChatDto: CreateChatDto, user: IUser) {
    try {
      const { partner } = createChatDto;
      const chats = await this.chatsRepository.find({
        relations: ['members'],
      });
      const findChat = chats.find(
        (chat) =>
          [+partner, +user.id].includes(+chat.members[0].id) &&
          [+partner, +user.id].includes(+chat.members[1].id),
      );
      if (findChat) {
        return findChat;
      }
      const user1 = await this.usersRepository.findOneBy({ id: partner });
      const user2 = await this.usersRepository.findOneBy({ id: user.id });

      const newChat = this.chatsRepository.create({
        lastMessageAt: new Date(),
      });
      newChat.addMember(user1);
      newChat.addMember(user2);
      await this.chatsRepository.save(newChat);

      newChat.members.forEach((member) => {
        this.pusherService.chatNew(member, newChat);
      });

      return newChat;
    } catch (error) {
      console.log(error);
      throw new BadRequestException('Server failure! Try again');
    }
  }

  findAll() {
    return `This action returns all chats`;
  }

  async findOne(id: number) {
    try {
      const chat = await this.chatsRepository.findOne({
        where: {
          id,
        },
        relations: ['members.company'],
      });
      if (chat) return chat;
      throw new HttpException('Bad request', HttpStatus.BAD_REQUEST);
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async findUserChats(currentPage: number, limit: number, user: IUser) {
    const defaultLimit = +limit ? +limit : 10;
    const offset = ((+currentPage || 1) - 1) * +defaultLimit;
    const totalItems = (
      await this.chatsRepository.find({
        where: {
          members: { id: +user.id },
        },
      })
    ).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);

    try {
      const chats = await this.chatsRepository.find({
        order: { lastMessageAt: 'DESC' },
        skip: offset,
        take: defaultLimit,
        relations: ['members.company', 'messages.seenBy'],
      });
      const findChat = chats.filter(
        (chat) =>
          +user.id === chat.members[0].id || +user.id === chat.members[1].id,
      );
      return {
        data: findChat,
        meta: {
          current: currentPage || 1,
          pageSize: defaultLimit,
          pages: totalPages,
          total: totalItems,
        },
      };
    } catch (error) {
      console.log(error);
      throw new BadRequestException('Server failure! Try again');
    }
  }

  async seen(id: number, user: IUser) {
    try {
      const chat = await this.chatsRepository.findOne({
        where: { id },
        relations: ['messages.seenBy', 'members'],
      });
      const lastMessage = chat.messages[chat.messages.length - 1];

      if (!lastMessage) return chat;

      const updatedMessage = await this.messagesRepository.findOne({
        where: {
          id: lastMessage.id,
        },
        relations: ['seenBy'],
      });
      updatedMessage.addUserSeen(user);
      await this.messagesRepository.save(updatedMessage);

      this.pusherService.chatUpdate(user, `${chat.id}`, updatedMessage);

      if (lastMessage.seenBy.find((u) => +u.id === +user.id)) {
        return chat;
      }

      this.pusherService.messagesUpdate(`${chat.id}`, updatedMessage);

      return updatedMessage;
    } catch (error) {
      console.log(error);
      throw new BadRequestException('Server failure! Try again');
    }
  }

  remove(id: number) {
    return `This action removes a #${id} chat`;
  }
}
