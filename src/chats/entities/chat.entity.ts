import { Message } from '@/messages/entities/message.entity';
import { User } from '@users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('chats')
export class Chat {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'datetime' })
  lastMessageAt: Date;

  @ManyToMany(() => User, (user) => user.chats)
  @JoinTable()
  members: User[];

  addMember(user: User) {
    if (this.members === undefined || this.members === null) {
      this.members = new Array<User>();
    }
    this.members.push(user);
  }

  @OneToMany(() => Message, (mess) => mess.chat)
  messages: Message[];

  @CreateDateColumn({ nullable: true })
  createdAt: Date;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @DeleteDateColumn({ nullable: true })
  deletedAt: Date;
}
