import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { ChatsService } from './chats.service';
import { CreateChatDto } from './dto/create-chat.dto';
import { ApiTags } from '@nestjs/swagger';
import { User } from '@/decorator/user';
import { IUser } from '@users/users.interface';

@ApiTags('chats')
@Controller('chats')
export class ChatsController {
  constructor(private readonly chatsService: ChatsService) {}

  @Post()
  create(@Body() createChatDto: CreateChatDto, @User() user: IUser) {
    return this.chatsService.create(createChatDto, user);
  }

  @Get()
  findAll() {
    return this.chatsService.findAll();
  }

  @Get('me')
  findUserChats(
    @Query('page') currentPage: string,
    @Query('limit') limit: string,
    @User() user: IUser,
  ) {
    return this.chatsService.findUserChats(+currentPage, +limit, user);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.chatsService.findOne(+id);
  }

  @Post(':id/seen')
  seen(@Param('id') id: string, @User() user: IUser) {
    return this.chatsService.seen(+id, user);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.chatsService.remove(+id);
  }
}
