import { Module } from '@nestjs/common';
import { ChatsService } from '@chats/chats.service';
import { ChatsController } from '@chats/chats.controller';
import { Chat } from '@chats/entities/chat.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '@users/entities/user.entity';
import { Message } from '@/messages/entities/message.entity';
import { Company } from '@companies/entities/company.entity';
import { PusherService } from '@/pusher/pusher.service';

@Module({
  imports: [TypeOrmModule.forFeature([Chat, User, Message, Company])],
  controllers: [ChatsController],
  providers: [ChatsService, PusherService],
  exports: [ChatsService],
})
export class ChatsModule {}
