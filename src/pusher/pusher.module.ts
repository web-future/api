import { Module } from '@nestjs/common';
import { PusherService } from '@/pusher/pusher.service';
import { PusherController } from '@/pusher/pusher.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Company } from '@companies/entities/company.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Company])],
  controllers: [PusherController],
  providers: [PusherService],
})
export class PusherModule {}
