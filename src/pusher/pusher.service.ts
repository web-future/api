import { Message } from '@/messages/entities/message.entity';
import { Chat } from '@chats/entities/chat.entity';
import { Company } from '@companies/entities/company.entity';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { IUser } from '@users/users.interface';
import Pusher from 'pusher';
import { Repository } from 'typeorm';

@Injectable()
export class PusherService {
  private pusher: Pusher;

  constructor(
    private configService: ConfigService,
    @InjectRepository(Company)
    private companiesRepository: Repository<Company>,
  ) {
    this.pusher = new Pusher({
      appId: '1845140',
      key: 'fbfb409494432654e2fa',
      secret: '793637769bdb4d694ba1',
      cluster: 'ap1',
      useTLS: true,
    });
  }

  authentication(socket_id: string, u: IUser) {
    const user = {
      id: `${u.id}`,
      user_info: {
        name: `${u.fullname}`,
      },
    };
    const authResponse = this.pusher.authenticateUser(socket_id, user);
    return authResponse;
  }

  async sendNotification(
    id: number,
    recipientId: number,
    senderId: number,
    status: number,
  ) {
    const sender = await this.companiesRepository.findOneBy({ id: senderId });
    await this.pusher.trigger(`${recipientId}`, 'getNoti', {
      id,
      status,
      createdAt: new Date(),
      sender,
    });
  }

  async messagesNew(chatId: string, newMessage: Message) {
    await this.pusher.trigger(`${chatId}`, 'messages:new', newMessage);
  }

  async chatUpdate(user: IUser, chatId: string, lastMessage: Message) {
    await this.pusher.trigger(`${user.email}`, 'chat:update', {
      id: chatId,
      messages: [lastMessage],
    });
  }

  async messagesUpdate(chatId: string, updateMessage: Message) {
    await this.pusher.trigger(`${chatId}`, 'messages:update', updateMessage);
  }

  async chatNew(user: IUser, newChat: Chat) {
    await this.pusher.trigger(`${user.email}`, 'chat:new', newChat);
  }
}
