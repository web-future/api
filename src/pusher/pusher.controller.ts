import { Body, Controller, Post } from '@nestjs/common';
import { PusherService } from './pusher.service';
import { User } from '@/decorator/user';
import { IUser } from '@users/users.interface';

@Controller('pusher')
export class PusherController {
  constructor(private readonly pusherService: PusherService) {}

  @Post('user-auth')
  authentication(@Body('socket_id') socket_id: string, @User() user: IUser) {
    return this.pusherService.authentication(socket_id, user);
  }
}
