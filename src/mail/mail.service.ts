import { MailerService } from '@nestjs-modules/mailer';
import { BadRequestException, Injectable } from '@nestjs/common';

@Injectable()
export class MailService {
  constructor(private mailerService: MailerService) {}

  private postcodeList = [];

  async sendVerifyEmail(email: string, fullname: string) {
    try {
      const ramdomPostcode = Math.floor(Math.random() * 10000);
      this.postcodeList.push(ramdomPostcode);

      await this.mailerService.sendMail({
        to: email,
        from: 'MyJob <info.myjob.contact@gmail.com>',
        subject: 'Verify your email!!',
        template: 'verify-email',
        context: {
          recipient: fullname,
          postcode: ramdomPostcode,
        },
      });
    } catch (error) {
      throw new BadRequestException('Server failure! Try again');
    }
  }

  verifyPostcode(postcode: number) {
    if (this.postcodeList.find((number) => number === postcode)) {
      this.postcodeList = this.postcodeList.filter(
        (number) => number !== postcode,
      );
      return true;
    }
    return false;
  }
}
